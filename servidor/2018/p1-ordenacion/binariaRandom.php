<?php
function numberGenerator($len_arrayumber) { // Generador de numeros random
    $array = array();
    for ($i=0; $i < $len_arrayumber; $i++) {
        $random_number = random_int(100, 999); // asignamos un random_int a la variable
        array_push($array, $random_number); // y hacemos un push al array
    }
    return $array; // devolvemos el array
}

// Funcion binaria.
function binary($array, $len_array) {
    for ($i=1; $i < $len_array; $i++) { // Por cada elemento de la lista
        $first = $array[$i]; // Asignamos el primer elemento
        $left = 0; // El elemento a la izquierda
        $right = $i-1; // Y el elemento a la derecha

        while ($left <= $right) { // Mientras el numero a la izquierda es menor que el a la derecha
            $average = (($left + $right) / 2); // Creamos el numero average
            if ($first < $array[$average]) { // Si el average es mayor que el primero
                $right = $average - 1; // el numero a la derecha = average - 1
            } else {
                $left = $average + 1; // el numero a la izquirda = average + 1
            }
        }

        $previous = $i-1;
        while ($previous >= $left) { // si el numero anterior es menor que el a la izquierda
            $array[$previous + 1] = $array[$previous]; // el numero actual (anterior +1) es igual al anterior
            $previous = $previous - 1; // el numero anterior es igual al actual (anterior -1)
        }
        $array[$left] = $first; // el numero a la izquierda = primero
    }

    return $array;
}

function initiate() { // Inicializa la funcion principal
    $binary_array = numberGenerator(100); // llama la funcion de generar numeros aleatorios
                                                        // y genera 100 numeros.
    $function = binary($binary_array, sizeof($binary_array)); // llama la funcion principal, usando el array con
                                                              // numeros aleatorios.

    for ($i = 1; $i <= sizeof($function); $i++) { // Imprime en pantalla el contenido del array.
        if ($i == 100) { // Para el ultimo numero, acabar en ".", en vez de ",".
            echo $function[$i - 1] . ".";
        } else if ($i % 10 == 0) { // Cada decimo caracter - echo <br> para formar una tabla de 10x10.
            echo $function[$i - 1] . ", ";
            echo '<br>';
        } else {
            echo $function[$i - 1] . ", "; // Imprime todos los numeros, seguidos por ", ".
        }
    }
}

initiate(); // Llama la funcion initiate().
?>