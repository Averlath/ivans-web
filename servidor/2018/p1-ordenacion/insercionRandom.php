<?php
function numberGenerator($len_arrayumber) { // Generador de numeros random
    $array = array();
    for ($i=0; $i < $len_arrayumber; $i++) {
        $random_number = random_int(100, 999); // asignamos un random_int a la variable
        array_push($array, $random_number); // y hacemos un push al array
    }
    return $array; // devolvemos el array
}

// Funcion Direct Insert
function directInsert($array, $len_array) {
    for ($i = 1; $i < $len_array; $i++) { // recorremos la lista
        $first = $array[$i]; // Asignamos los valores first
        $next = $i - 1; // y next
        while ($next >= 0 && $array[$next] > $first) { // while loop
            $array[$next + 1] = $array[$next]; // asignamos el next + 1 a next
            $next--;
        }
        $array[$next + 1] = $first; // asignamos el next + 1 a first
    }
    return $array;
    }

function initiate() { // Inicializa la funcion principal
    $binary_array = numberGenerator(100); // llama la funcion de generar numeros aleatorios
    // y genera 100 numeros.
    $function = directInsert($binary_array, sizeof($binary_array)); // llama la funcion principal, usando el array con
    // numeros aleatorios.

    for ($i = 1; $i <= sizeof($function); $i++) { // Imprime en pantalla el contenido del array.
        if ($i == 100) { // Para el ultimo numero, acabar en ".", en vez de ",".
            echo $function[$i - 1] . ".";
        } else if ($i % 10 == 0) { // Cada decimo caracter - echo <br> para formar una tabla de 10x10.
            echo $function[$i - 1] . ", ";
            echo '<br>';
        } else {
            echo $function[$i - 1] . ", "; // Imprime todos los numeros, seguidos por ", ".
        }
    }
}

initiate(); // Llama la funcion initiate().
?>
