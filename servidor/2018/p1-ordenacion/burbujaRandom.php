<?php
function numberGenerator($number) { // Generador de numeros random
    $array = array();
    for ($i=0; $i < $number; $i++) {
        $random_number = random_int(100, 999); // asignamos un random_int a la variable
        array_push($array, $random_number); // y hacemos un push al array
    }
    return $array; // devolvemos el array
}


// Funcion bubble
function bubble($bubble_array) {
    while (true) { // un while loop infinito
        $counter = 0; // creamos un contador
        for ($i = 0; $i < count($bubble_array) - 1; $i++) { // recorremos el array
            $first = $bubble_array[$i]; // asignamos el primer valor
            $next = $bubble_array[$i + 1]; // y el proximo valor
            if ($next < $first) { // si el primero valor es mayor que el proximo
                $swapper = $bubble_array[$i]; // swapper es una variable de ayuda al intercambio de valores
                // en nuestro caso, guarda el valor de $first para utilizar mas tarde
                $bubble_array[$i] = $next; // el valor del elemento i = proximo
                $bubble_array[$i + 1] = $swapper; // Utilizando el swapper, cambiamos el valor de next al valor de swapper ($first)
                $counter++; // aumentamos el contador.
            }
        }

        if ($counter === 0) { // En caso de que el contador es igual a 0, significa que no ha hecho ningun cambio al array
            break; // y en ese caso, ejecutamos break para salir del while loop infinito
        }


        return $bubble_array;
    }
}


function initiate() { // Inicializa la funcion principal
    $binary_array = numberGenerator(100); // llama la funcion de generar numeros aleatorios
    // y genera 100 numeros.
    $function = bubble($binary_array); // llama la funcion principal, usando el array con
    // numeros aleatorios.

    for ($i = 1; $i <= sizeof($function); $i++) { // Imprime en pantalla el contenido del array.
        if ($i == 100) { // Para el ultimo numero, acabar en ".", en vez de ",".
            echo $function[$i - 1] . ".";
        } else if ($i % 10 == 0) { // Cada decimo caracter - echo <br> para formar una tabla de 10x10.
            echo $function[$i - 1] . ", ";
            echo '<br>';
        } else {
            echo $function[$i - 1] . ", "; // Imprime todos los numeros, seguidos por ", ".
        }
    }
}

initiate(); // Llama la funcion initiate().
?>