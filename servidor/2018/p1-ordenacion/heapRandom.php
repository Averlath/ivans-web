<?php
function numberGenerator($len_arrayumber) { // Generador de numeros random
    $array = array();
    for ($i=0; $i < $len_arrayumber; $i++) {
        $random_number = random_int(100, 999); // asignamos un random_int a la variable
        array_push($array, $random_number); // y hacemos un push al array
    }
    return $array; // devolvemos el array
}

function heapsort($array, $len_array) {
    for ($k=$len_array-1; $k >= 0; $k--) { // recorremos la lista
        for ($i=1; $i <= $k; $i++) { // otro for loop, mientras i < k
            $first = $array[$i]; // asignamos las variables de primero
            $half = $i/2; // y el valor de i dividido entre 2.
            while ($half > 0 && $array[$half] < $first) { // mientras el valor medio es mas que 0 y
                                                          // menor que $first
                $array[$i] = $array[$half]; // i == half
                $i = $half;
                $half = $half/2;
            }
            $array[$i]=$first;
        }
        $swapper = $array[0]; // Usamos swapper para guardar el valor de array[0]
        $array[0] = $array[$k]; // asignamos el valor de $k al array[0]
        $array[$k] = $swapper; // asignamos el valor de array[0] guardado al array[$k]
    }
    return $array;
}

function initiate() { // Inicializa la funcion principal
    $binary_array = numberGenerator(100); // llama la funcion de generar numeros aleatorios
    // y genera 100 numeros.
    $function = heapsort($binary_array, sizeof($binary_array)); // llama la funcion principal, usando el array con
    // numeros aleatorios.

    for ($i = 1; $i <= sizeof($function); $i++) { // Imprime en pantalla el contenido del array.
        if ($i == 100) { // Para el ultimo numero, acabar en ".", en vez de ",".
            echo $function[$i - 1] . ".";
        } else if ($i % 10 == 0) { // Cada decimo caracter - echo <br> para formar una tabla de 10x10.
            echo $function[$i - 1] . ", ";
            echo '<br>';
        } else {
            echo $function[$i - 1] . ", "; // Imprime todos los numeros, seguidos por ", ".
        }
    }
}

initiate(); // Llama la funcion initiate().
?>