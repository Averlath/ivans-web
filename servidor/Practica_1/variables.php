<!DOCTYPE html>
<html>
<head>
	<title>Tabla de Multiplicar</title>
</head>

<body>

	<?php

		//error_reporting(E_ALL);

		$list = ['$var = null', '$var = 0', '$var = true', '$var = false', '$var = "0"', '$var = ""', '$var = "foo"', '$var = array()', 'unset ($var)'];
		$setter = [null, 0, true, false, "0", "", "foo", array()];

		echo "<table border='1' style='margin: 10px; padding: 5px;'>";
		echo '<tr><th style="padding: 5px;">Estado de $var</th><th style="padding: 5px;">isset($var)</th><th style="padding: 5px;">empty($var)</th><th style="padding: 5px;">(bool) $var</th></tr>';
		echo "<tr>";

		for ($i = 0; $i < sizeof($list); $i ++) {
			if ($i === sizeof($list) - 1) {
				unset($item);
			} else {
				$item = $setter[$i];
			}

			echo "<tr style='text-align: center;'>";
			echo "<td style='padding: 5px;'>" . $list[$i] . "</td>";

			if (isset($item) == true) {
				echo "<td> True </td>";
			} else {
				echo "<td> False </td>";
			}

			if (empty($item) == true) {
				echo "<td> True </td>";
			} else {
				echo "<td> False </td>";
			}

			if ((bool) $item == true) {
				echo "<td> True </td>";
			} else {
				echo "<td> False </td>";
			}

			echo "</tr>";
		}

		echo "</tr>";
		echo "</table>";

	?>
</body>
</html>