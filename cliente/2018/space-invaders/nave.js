var canvas = document.createElement("canvas");
canvas.height = window.innerHeight-50;
canvas.width = window.innerWidth-50;
document.body.insertBefore(canvas, document.body.childNodes[0]);

var enemies=[];
for (i=0; i<5; i++) {
	//if (enemies[i].posX <= window.innerWidth*0.9-50) {
	enemies[i] = new Ship(window.innerWidth*0.1+70*i, 70, 50, 50, "green");
	//}
}
var ship = new Ship(window.innerWidth/2, window.innerWidth-150, 50, 50, "red");
var ammo = [];
var buttons = [];

window.onkeydown = function (e) {
	buttons[e.keyCode] = true;
}

window.onkeyup = function (e) {
	buttons[e.keyCode] = false;
}

function Ship(posX, posY, sizeX, sizeY, color) {
	this.posX = posX;
	this.posY = posY;
	this.sizeX = sizeX;
	this.sizeY = sizeY;
	this.color = color;
	this.pintar = function() {
		var context = canvas.getContext("2d");
		context.fillStyle = this.color;
		context.fillRect(this.posX, this.posY, this.sizeX, this.sizeY);
	};
}

function areaUpdate() {
	if (buttons[39]) {
		if (ship.posX < canvas.width) {
			ship.posX += 10;
		} else {
			ship.posX = canvas.width-ship.sizeX;
		}
	}
	if (buttons[37]) {
		if (ship.posX > 0) {
			ship.posX -= 10;
		} else {
			ship.posX = 0;
		}
	}
	if (buttons[32]) {
		if (ammo.length==0 || (ammo[ammo.length-1].posY < ship.posY-70)) {
			ammo[ammo.length] = new Ship(ship.posX + ship.sizeX/2-2.5, ship.posY, 5, 5, "black");
		}
	}
	var context = canvas.getContext("2d");
	context.clearRect(0, 0, canvas.width, canvas.height)
	ship.pintar();

	for (i=0; i<enemies.length; i++) {
		if (enemies[i].posX <= window.innerWidth-55) {
			enemies[i].posX = enemies[i].posx + 5;
		} else {
			enemies[i].posX = window.innerWidth-50;
		}
		enemies[i].pintar();
	}

	for (i=0; i<ammo.length; i++) {
		ammo[i].posY -= 1;
		ammo[i].pintar();
		console.log("yes");
	}
}

var inter = setInterval(areaUpdate, 20);