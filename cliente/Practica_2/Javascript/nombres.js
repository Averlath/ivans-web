// Declaracion de variables.
let textfield, result;

window.onload = function() {
	result = document.getElementById("result");

	document.getElementById("bttn_entera").onclick = function() { // 1. Part sencera.
		obtenerText("text");
		entera(textfield, result);
	}

	document.getElementById("bttn_tres_decimales").onclick = function() { // 2. Amb 3 decimals.
		obtenerText("text");
		decimales(textfield, result);
	}

	document.getElementById("bttn_solo_decimales").onclick = function() { // 3. Solo 3 decimales.
		obtenerText("text");
		solo_decimales(textfield, result);
	}
}

const obtenerText = (id) => textfield = document.getElementById(id);
const entera = (t, r) => r.innerHTML = parseInt(t.value);
const decimales = (t, r) => r.innerHTML = (t.value / 125).toFixed(3);
const solo_decimales = (t, r) => r.innerHTML = t.value.substring(t.value.indexOf(".") + 1);