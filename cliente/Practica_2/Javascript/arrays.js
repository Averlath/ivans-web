// Declaracion de variables.
let textfield, result;

window.onload = function() {
	result = document.getElementById("result");

	document.getElementById("bttn_order_a").onclick = function() { // 1. Ordenar de menor a mayor.
		obtenerText("text");
		orderA(textfield, result);
	}
	document.getElementById("bttn_order_b").onclick = function() { // 2. Ordenar de mayor a menor.
		obtenerText("text");
		orderB(textfield, result);
	}
	document.getElementById("bttn_sumar").onclick = function() { // 3. Sumar contenido del array.
		obtenerText("text");
		sumar(textfield, result);
	}
	document.getElementById("bttn_sumar_dos").onclick = function() { // 4. Sumar con un bucle.
		obtenerText("text");
		sumarDos(textfield, result);
	}
	document.getElementById("bttn_max").onclick = function() { // 5. Obtener numero maximo.
		obtenerText("text");
		max(textfield, result);
	}
	document.getElementById("bttn_max_dos").onclick = function() { // 6. Obtener numero maximo usando forEach().
		obtenerText("text");
		maxDos(textfield, result);
	}
	document.getElementById("bttn_numeros").onclick = function() { // 7. Comprobar que todos son numeros.
		obtenerText("text");
		numeros(textfield, result);
	}
	document.getElementById("bttn_random").onclick = function() { // 8. Crear 5 arrays con los numeros en orden random.
		obtenerText("text");
		random(textfield, result);
	}
	document.getElementById("bttn_ampliar").onclick = function() { // 9. Cortar primeros 3 nums y poner al final, invertidos.
		obtenerText("text");
		ampliar(textfield, result);
	}
	document.getElementById("bttn_compuesto").onclick = function() { // 10. Unir los numeros y sumar 5.
		obtenerText("text");
		compuesto(textfield, result);
	}
}

const obtenerText = (id) => textfield = document.getElementById(id);
const orderA = (t, r) => r.innerHTML = t.value.split(",").sort(function(a, b) {return a-b});
const orderB = (t, r) => r.innerHTML = t.value.split(",").sort(function(a, b) {return b-a});
const sumar = (t, r) => r.innerHTML = t.value.split(",").reduce(function(a, b) {return parseInt(a)+parseInt(b)});
const sumarDos = (t, r) => {
	let arr = t.value.split(","), total = 0;

	for (let i = 0; i < arr.length; i++) {
		total += parseInt(arr[i]);
	}
	r.innerHTML = total;
}
const max = (t, r) => r.innerHTML = Math.max.apply(Math, t.value.split(","));
const maxDos = (t, r) => {
	let arr = t.value.split(","), largest = arr[0];

	arr.forEach(function(current) {
		if (parseInt(largest) < parseInt(current)) {
			largest = current;
		}
	});
	r.innerHTML = largest;
}
const numeros = (t, r) => {
	let arr = t.value.split(","), answer = true;
	arr.forEach(function (current) {
		if (isNaN(current)) {
			answer = false;
		}
	});
	answer === true ? r.innerHTML = "Son numeros." : r.innerHTML = "No son numeros.";
}
const random = (t, r) => {
	let arr = t.value.split(","), answer = [[], [], [], [], []];

	arr.forEach(function (current) {
		for (let i=0; i<answer.length; i++) {
			answer[i].push(current);
		}
	});

	for (let i=0; i<answer.length; i++) {
		answer[i].sort(function(a,b) { return 0.5 - Math.random() });
		r.innerHTML += answer[i] + "<br>";
	}
}
const ampliar = (t, r) => {
	let arr = t.value.split(","), empty = [];

	for (i = 0; i < 3; i++) {
		empty.push(arr[i]);
	}

	arr.splice(0, 3);
	empty.reverse();

	for (i = 0; i < empty.length; i++) {
		arr.push(empty[i]);
	}

	r.innerHTML = arr;
}
const compuesto = (t, r) => {
	let arr = t.value.split(","), numbers = "";

	arr.forEach(function (current) {
		numbers += current
	});
	r.innerHTML = parseInt(numbers) + 5;
}