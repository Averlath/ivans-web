// Declaracion de variables.
let textfield, result;

window.onload = function() {
	let result = document.getElementById("result");

	document.getElementById("bttn_order_a").onclick = function() { // 1. Ordenar alfabeticamente.
		obtenerText("text");
		orderA(textfield, result);
	}
	document.getElementById("bttn_order_b").onclick = function() { // 2. Ordenar por mas largo.
		obtenerText("text");
		orderB(textfield, result);
	}
	document.getElementById("bttn_calculo").onclick = function() { // 3. Crear otro array con el length de los strings.
		obtenerText("text");
		calculo(textfield, result);
	}
	document.getElementById("bttn_crear").onclick = function() { // 4. Crear frase separada por guiones a partir del array.
		obtenerText("text");
		crear(textfield, result);
	}
	document.getElementById("bttn_acronimo").onclick = function() { // 5. Acronimo.
		obtenerText("text");
		acronimo(textfield, result);
	}
	document.getElementById("bttn_filtro").onclick = function() { // 6. Borrar strings que tienen length menos de 5.
		obtenerText("text");
		filtro(textfield, result);
	}
	document.getElementById("bttn_ocurrencias").onclick = function() { // 7. Mostrar el numero de veces que "la" aparece.
		obtenerText("text");
		ocurrencias(textfield, result);
	}
	document.getElementById("bttn_ocurrencias_dos").onclick = function() { // 8. Mostrar el numero de palabras con 3+ letras.
		obtenerText("text");
		ocurrenciasB(textfield, result);
	}
	document.getElementById("bttn_restructurar").onclick = function() { // 9. Coger primer y ultimo elemento y ponerlo en medio.
		obtenerText("text");
		restructurar(textfield, result);
	}
}

const obtenerText = (id) => textfield = document.getElementById(id);
const orderA = (t, r) => r.innerHTML = t.value.split(",").sort();
const orderB = (t, r) => r.innerHTML = t.value.split(",").sort(function(a, b) {return b.length-a.length});
const calculo = (t, r) => {
	let arr = t.value.split(","), answer = [];

	arr.forEach(function(current) {
		answer.push(current.length);
	});
	r.innerHTML = answer;
}
const crear = (t, r) => {
	let arr = t.value.split(","), answer = "";

	arr.forEach(function(current) {
		answer += current + "-";
	});
	r.innerHTML = answer.slice(0, -1);
}
const acronimo = (t, r) => {
	let arr = t.value.split(","), answer = "";

	arr.forEach(function(current) {
		answer += current.replace(/ /g, "").charAt(0).toUpperCase();
	});
	r.innerHTML = answer;
}
const filtro = (t, r) => {
	let arr = t.value.split(","), answer = [];

	for (i = 0; i < arr.length; i++) {
		if (arr[i].length > 5) {
			answer.push(arr[i]);
		}
	}
	r.innerHTML = answer;
}
const ocurrencias = (t, r) => {
	let arr = t.value.split(","), answer = 0;

	arr.forEach(function(current) {
		if (current.match(/la/g) !== null) {
			answer += current.match(/la/g).length;
		}
	});
	r.innerHTML = answer;
}
const ocurrenciasB = (t, r) => {
	let arr = t.value.split(","), answer = 0;

	arr.forEach(function(current) {
		if (current.length > 3) {
			answer++;
		}
	});
	r.innerHTML = answer;
}
const restructurar = (t, r) => {
	let arr = t.value.split(","), length = (arr.length - 2) / 2, new_arr = [], helper = [];

	for (let i=0; i<arr.length; i++) {
		if (i === 0 || i === arr.length - 1) {
			helper.push(arr[i]);
		}
	}

	arr.shift();
	arr.pop();

	for (let i=0; i<arr.length; i++) {
		if (i === length) {
			for (let j=0; j<2; j++) {
				new_arr.push(helper[j]);
			}
		}
		new_arr.push(arr[i]);
	}
	r.innerHTML = new_arr;
}