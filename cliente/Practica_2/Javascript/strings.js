// Declaracion de variables.
var textfield, result;

window.onload = function() {
	result = document.getElementById("result");

	document.getElementById("bttn_vacio").onclick = function() { // 1. Vacio o no.
		obtenerText("text");
		vacio(textfield, result);
	}
	document.getElementById("bttn_extension").onclick = function() { // 2. Extension.
		obtenerText("text");
		extension(textfield, result);
	}
	document.getElementById("bttn_invertir").onclick = function() { // 3. Invertir.
		obtenerText("text");
		invertir(textfield, result);
	}
	document.getElementById("bttn_contar").onclick = function() { // 4. Contar ocurrencias de "a".
		obtenerText("text");
		contar(textfield, result);
	}
	document.getElementById("bttn_first_last").onclick = function() { // 5. Primera y ultima "a".
		obtenerText("text");
		first_last(textfield, result);
	}
	document.getElementById("bttn_inbetween").onclick = function() { // 6. Texto entre primera y segunda "a".
		obtenerText("text");
		inbetween(textfield, result);
	}
	document.getElementById("bttn_borrar").onclick = function() { // 7. Borrar todas las "la".
		obtenerText("text");
		borrar(textfield, result);
	}
	document.getElementById("bttn_substituir").onclick = function() { // 8. Substituir todas las "la" por "*".
		obtenerText("text");
		substituir(textfield, result);
	}
	document.getElementById("bttn_capicua").onclick = function() { // 9. Primera letra delante y detras del string.
		obtenerText("text");
		capicua(textfield, result);
	}
	document.getElementById("bttn_espacios").onclick = function() { // 10. Quitar todos los espacios.
		obtenerText("text");
		espacios(textfield, result);
	}
	document.getElementById("bttn_frase").onclick = function() { // 11. Substituir todos los espacios por 1 "-".
		obtenerText("text");
		frase(textfield, result);
	}
	document.getElementById("bttn_acronimo").onclick = function() { // 12. Arconimo.
		obtenerText("text");
		acronimo(textfield, result);
	}
	document.getElementById("bttn_palindromo").onclick = function() { // 12. Comprobar si el string es un palindromo.
		obtenerText("text");
		palindromo(textfield, result);
	}
	document.getElementById("bttn_email").onclick = function() { // 13. Email con comprobaciones.
		obtenerText("text");
		email(textfield, result);
	}
}

const obtenerText = (id) => textfield = document.getElementById(id);	
const vacio = (t, r) => t.value === "" ? r.innerHTML = "Vacio." : r.innerHTML = "No es vacio.";  
const extension = (t, r) => {
	let extension = t.value.substring(t.value.lastIndexOf(".") + 1);
	switch (extension) {
		case "txt": r.innerHTML = "Extension: txt"; break;
		case "exe": r.innerHTML = "Extension: exe"; break;
		case "jpg": r.innerHTML = "Extension: jpg"; break;
		default: r.innerHTML = "Extension desconocida";
	}
}
const invertir = (t, r) => r.innerHTML = t.value.split("").reverse().join("");
const contar = (t, r) => r.innerHTML = t.value.split('a').length-1;
const first_last = (t, r) => r.innerHTML = t.value.indexOf("a") + " " + t.value.lastIndexOf("a");
const inbetween = (t, r) => {
	let intial = t.value.substring(t.value.indexOf("a") + 1);	
	r.innerHTML = intial.substring(0, intial.indexOf("a"));
}
const borrar = (t, r) => r.innerHTML = t.value.replace(/la/g, "");
const substituir = (t, r) => r.innerHTML = t.value.replace(/la/g, "*");
const capicua = (t, r) => r.innerHTML = t.value.charAt(0) + t.value + t.value.charAt(0);
const espacios = (t, r) => r.innerHTML = t.value.replace(/ /g, "");
const frase = (t, r) => r.innerHTML = t.value.replace(/ +/g, "-");
const acronimo = (t, r) => {
	let arr = t.value.split(" "), result = "";
	for (let i = 0; i < arr.length; i++) {
		result += arr[i].charAt(0).toUpperCase();
	}
	r.innerHTML = result;
}
const palindromo = (t, r) => t.value === t.value.split("").reverse().join("") ? r.innerHTML = "Si." : r.innerHTML = "No.";
const email = (t, r) => {
	let extensiones = [".net", ".org", ".com"], correo = false;
	if (t.value.indexOf("@") > 0) {
		if (t.value.lastIndexOf(".") > t.value.indexOf("@")) {
			if (extensiones.includes(t.value.substr(t.value.length - 4))) {
				correo = true;
			}
		}
	}
	correo === true ? r.innerHTML = "Si, el correo es correcto." : r.innerHTML = "No, el correo no es correcto.";
}