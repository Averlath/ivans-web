var time;

window.onload = function() {
	document.getElementById("name").style.display = "none";

	document.getElementById("begin").onclick = function() {
		let nickname   = getNickname(),
			difficulty = document.getElementById("difficulty").value,
			element    = document.getElementById("postit");
		begin(nickname, difficulty, element);
	}
}

function getNickname() {
	let text = document.getElementById("nickname");
	if (text.value === "") {
		return "Annonymous";
	} else {
		return text.value;
	}
}

function begin(nickname, difficulty, element) {

	document.getElementById("name").innerHTML = "Player: " + nickname;
	document.getElementById("name").style.display = "block";
	document.getElementById("info").style.display = "none";

	let cards = [],
	    numbers = [],
	    images = [];

	cards = obtainDifficulty(difficulty, cards);
	
	generateRandomNumber(cards[0], numbers, cards[2]);

	obtainImages(numbers, images);

	generateElements(cards, element, images);

	startTimer(time, cards[0]);
}

function obtainDifficulty(difficulty) {
	switch (difficulty) {
		case "easy":
			time = 120;
			return [8, 4, 2];
		case "medium":
			time = 240;
			return [16, 8, 2];
		case "hard":
			time = 480;
			return [24, 8, 2];
		case "very hard":
			time = 960;
			return [36, 9, 3];
		default:
			console.log("Error");
	}
}

function generateRandomNumber(cards, arr, pairs) {
	while (arr.length !== cards) {
		number = Math.floor(Math.random() * 12) + 1;
		if (!arr.includes(number)) {
			for (let i = 0; i < pairs; i++) {
				arr.push(number);
			}
		}
	}
	shuffleArray(arr);
}

function shuffleArray(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
    return arr;
}

function obtainImages(numbers, images) {
	for (element of numbers) {
		images.push("images/" + element + ".png");
	}
}

function generateElements(cards, element, images) {
	element.innerHTML = "";
	for (let i = 0; i < cards[0]; i++) {
		if (i % cards[1] === 0 && i !== 0) {
			element.innerHTML += "<br><br><br><br><br><br><br><br>";
		}
		let box = document.createElement("img");
		box.setAttribute("class", "images");
		box.setAttribute("id", "n" + i);
		box.setAttribute('src', 'images/back.png')
		
		element.appendChild(box);
	}

	manageOnClick(images, cards[2]);
}

function manageOnClick(arr_images, pairs) {
	let images = document.getElementsByClassName("images"),
		clicked = [];

	for (let i = 0; i < images.length; i++) {
		image = images[i];
        image.onclick = function() {
        	if (this.correct !== true) {
	        	if (clicked.length < pairs) {
	        		this.setAttribute('src', arr_images[i]);
	        		clicked.push(this);
	        	}

	        	if (clicked.length === pairs) {
	        		if (clicked[0].src === clicked[1].src) {
	        			if (pairs === 3) {
	        				if (clicked[1].src === clicked[2].src) {
	        					clicked[0].correct = true;
			        			clicked[1].correct = true;
		        				clicked[2].correct = true;
	        				}
	        			}
	        			clicked[0].correct = true;
	        			clicked[1].correct = true;
	        			if (pairs === 3) {
	        				clicked[2].correct = true;
	        			}
	        			setTimeout(function() {
	        				clicked = [];
	        			}, 1000);
	        		} else {
	        			setTimeout(function() {
	        				clicked[0].src = "images/back.png";
	        				clicked[1].src = "images/back.png";
	        				if (pairs === 3) {
	        					clicked[2].src = "images/back.png";
	        				}
	        				clicked = [];
	        			}, 2000);
	        		}
	        	}
	        }
        }
    }
}

function startTimer(time, cards) {
    let minute = time / 60 - 1,
    	second = 59,
    	correct;

    setInterval(function() {
    	let images = document.getElementsByClassName("images");

    	for (let i = 0; i < images.length; i++) {
			image = images[i];
			if (image.correct === true) {
				correct += 1;
			}
	    }

	    if (correct === cards) {
	    	document.getElementById("timer").style.display = "none";
	    	document.getElementById("win").innerHTML = "You Win";
	    } else {
	    	correct = 0;
	    }

    	if (second < 10) {
	    	document.getElementById("timer").innerHTML = minute + ":0" + second;
        } else {
	    	document.getElementById("timer").innerHTML = minute + ":" + second;
        }

        if (second == 0) {
		    minute--;
		    second = 59;
        }

    	if (minute == 0 && second == 0) {
	    	endGame();
        }

        second--;
    }, 1000);
}

function endGame() {
	alert("You lose");
}