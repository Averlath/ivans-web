window.onload = function() {
	document.getElementById("name").style.display = "none";

	document.getElementById("begin").onclick = function() {
		let nickname   = getNickname(),
			difficulty = document.getElementById("difficulty").value,
			element    = document.getElementById("postit");
		begin(nickname, difficulty, element);
	}
}

function getNickname() {
	let text = document.getElementById("nickname");
	if (text.value === "") {
		return "Annonymous";
	} else {
		return text.value;
	}
}

function begin(nickname, difficulty, element) {

	document.getElementById("name").innerHTML = "Player: " + nickname;
	document.getElementById("name").style.display = "block";
	document.getElementById("info").style.display = "none";

	let cards = [],
	    numbers = [],
	    images = [];

	cards = obtainDifficulty(difficulty, cards);

	generateRandomNumber(cards[0], numbers);

	obtainImages(numbers, images);

	generateElements(cards, element, images);

	console.log(images);
}

function obtainDifficulty(difficulty) {
	switch (difficulty) {
		case "easy":
			return [8, 4];
		case "medium":
			return [16, 8];
		case "hard":
			return [24, 8];
		case "very hard":
			return [36, 9];
		default:
			console.log("Error");
	}
}

function generateRandomNumber(times, arr) {
	let i = times;
	while (i !== 0) {
		number = Math.floor(Math.random() * 12) + 1;
		if (!arr.includes(number)) {
			arr.push(number);
			arr.push(number);
		}
		i--;
	}
	shuffleArray(arr);
}

function shuffleArray(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
    return arr;
}

function obtainImages(numbers, images) {
	for (element of numbers) {
		images.push("images/" + element + ".png");
	}
}

function generateElements(cards, element, images) {
	element.innerHTML = "";
	for (let i = 0; i < cards[0]; i++) {
		if (i % cards[1] === 0 && i !== 0) {
			element.innerHTML += "<br><br><br><br><br><br><br><br>";
		}
		let casilla = document.createElement("img");
		casilla.setAttribute("class", "images");
		casilla.setAttribute("id", "n" + i);
		casilla.setAttribute('src', 'images/back.png')
		
		element.appendChild(casilla);
	}

	manageOnclick(images);
}

function manageOnclick(arr_images) {
	let images = document.getElementsByClassName("images"),
		clicked = [],
		length = arr_images === 36 ? 3 : 2;

	for (let i = 0; i < images.length; i++) {
		image = images[i];
        image.onclick = function() {
        	if (this.correct !== true) {
	        	if (clicked.length < 2) {
	        		this.setAttribute('src', arr_images[i]);
	        		clicked.push(this);
	        	}

	        	if (clicked.length === length) {
	        		if (clicked[0].src !== clicked[1].src) {
	        			setTimeout(function() {
	        				console.log(clicked);
	        				clicked[0].src = "images/back.png";
	        				clicked[1].src = "images/back.png";
	        				if (length === 3) {
	        					clicked[2].src = "images/back.png";
	        				}
	        				clicked = [];
	        			}, 2000);
	        		} else {
	        			clicked[0].correct = true;
	        			clicked[1].correct = true;
	        			if (length === 3) {
	        				clicked[2].correct = true;
	        			}
	        			setTimeout(function() {
	        				clicked = [];
	        			}, 1000);
	        		}
	        	}
	        }
        }
    }
}