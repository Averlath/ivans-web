class Block {
    x; y; size; walkable;

    constructor(x, y, size, walkable) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.walkable = walkable;
    }

    create(ctx, walkable) {
        if (walkable === true) {
            ctx.fillStyle = "f5f5f5";
        } else {
            ctx.fillStyle = "red";
        }
        
        ctx.fillRect(this.x * this.sizeX, this.y * this.sizeY,
            this.sizeX, this.sizeY);
    }
}