class Game {
	canvas;
	ctx;
	block_size = 40;
	map;
	block_map = [];
	walkable = 1;
	interval;

	constructor(map) {
		// Construct Canvas
		this.canvas = document.getElementById("game");
		this.ctx = this.canvas.getContext("2d");

		//Construct Map
		this.map = map;
		this.canvas.width = this.map[0].length * this.tile_size_X;
        this.canvas.height = this.map.length * this.tile_size_Y;

        for (let i = 0; i < map.length; i++) {
            let row = [];
            for (let j = 0; j < map[i].length; j++) {
                let is_walkable = this.walkable == map[i][j];
                row.push(new Block(j, i, this.block_size, is_walkable));
            }
            this.block_map.push(row);
        }

        this.interval = setInterval(() => { this.refresh(); }, 50);
	}

	refresh() {
		this.draw_map();
	}

    draw_map() {
        for (let i = 0; i < this.block_map.length; i++) {
            for (let j = 0; j < this.block_map[i].length; j++) {
                let block = this.block_map[i][j];
                console.log(block.walkable);
                block.create(this.ctx, block.walkable);
            }
        }
    }
}