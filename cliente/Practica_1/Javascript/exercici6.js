window.onload = function() {
	document.getElementById("enviar").onclick = function() {
		enviar();
	}
}

const enviar = () => {
	let message = document.getElementById("text").value, parent = document.getElementById("messages");

	if (message !== "") {
		var item = document.createElement("p");
		item.setAttribute("class", "message");
		item.innerHTML = message;
		item.onmouseover = function() {
			if (this.innerHTML.length > 10) {
				this.style.color = "green";
			} else {
				this.style.color = "red";
			}
			this.style.textDecoration = "underline";
		}
		item.onmouseout = function() {
			this.style.textDecoration = "none";
			this.style.color = "black";
		}
		parent.appendChild(item);

		document.getElementById("text").value = "";
	}
}


const checkType = () => {
	let item = document.getElementById("input").value, result = document.getElementById("result");

	if (item.replace(/\s/g, '') === "") {
		result.innerHTML = "No has esrito nada.";
	} else if (typeof parseInt(item) === "number" && isFinite(item)) {
		if (item % 1 === 0) {
			result.innerHTML = "El dato es de tipo Numero Entero.";
		} else {
			result.innerHTML = "El dato es de tipo Numero Decimal.";
		}
	} else if (typeof item === "string") {
		if (item.toLowerCase() === "true" || item.toLowerCase() === "false") {
			result.innerHTML = "El dato es de tipo Boolean.";
		} else {
			result.innerHTML = "El dato es de tipo String.";
		}
	}
}