window.onload = function() {
	$("#bn").removeAttr("style").hide();
}

const change = () => {
	const radios = document.getElementsByTagName('input');
	let value;
	for (let i = 0; i < radios.length; i++) {
	    if (radios[i].type === 'radio' && radios[i].checked) {
	        value = radios[i].value;       
	    }
	}

	if (value === "a") {
		$("#dropdown").removeAttr("style").hide();
		$("#pn").removeAttr("style").hide();
		$("#bn").show();
	} else {
		$("#dropdown").show();
		$("#pn").show();
		$("#bn").removeAttr("style").hide();
	}
}

