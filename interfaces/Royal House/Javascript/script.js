window.onload = function() { // Onload
	let url   = window.location.href,
		pages = ["Home", "Cinema", "Theatre", "Concerts"]
	active(pages, url);

	document.getElementById("login_form").style.display = "none";
	document.getElementById("search_form").style.display = "none";

	document.getElementById("today").innerHTML += getDate();
}

/* Active Class */
const active = (pages, url) => {
	let arr = url.split("/"),
		page = arr[arr.length-1].split(".")[0];

	document.getElementById(page).setAttribute('class', 'active');
}

/* Login Form */
const openForm = () => {
	closeSearch();
	document.getElementById("log").style.display = "none";
	document.getElementById("login_form").style.display = "block";
}

const closeForm = () => {
	document.getElementById("log").style.display = "block";
	document.getElementById("login_form").style.display = "none";
}

/* Search Form */
const openSearch = () => {
	closeForm();
	document.getElementById("search_form").style.display = "block";
}

const closeSearch = () => {
	document.getElementById("search_form").style.display = "none";
}

/* Current Date */
const getDate = () => {
	let today = new Date(),
		day = today.getDay(),
		dd = String(today.getDate()).padStart(2, '0'),
		mm = String(today.getMonth()).padStart(2, '0'),

		days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
		months = ["January", "February", "March", "April", "May", "June", "July", 
					"August", "September", "October", "November", "December"];
	
	return " - " + days[day] + ", " + months[mm] + " " + dd;
}